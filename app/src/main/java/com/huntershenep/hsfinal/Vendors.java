package com.huntershenep.hsfinal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.huntershenep.hsfinal.models.Vendor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Vendors extends AppCompatActivity {

    private static final String TAG = "Vendors";

    private Button btnStartAddVendors;
    private ListView theListView = null;
    private ArrayList<Vendor> allVendors = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors);

        btnStartAddVendors = findViewById(R.id.btnStartAddVendors);
        btnStartAddVendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Vendors.this, AddVendor.class);
                startActivity(i);
            }
        });

        theListView = findViewById(R.id.listViewVendors);
        populateListView();


    }

    public void clearValues(){
        allVendors.clear();
        theListView.removeAllViews();
    }
    private void populateListView(){

        SQLVendorDataAccess dataAccess = new SQLVendorDataAccess(this);
        allVendors = dataAccess.pullAllVendors();
        Log.d(TAG, "SIZE of allVendors: " + allVendors.size());


        ArrayAdapter<Vendor> adapter = new ArrayAdapter(this, R.layout.my_custom_layout, R.id.lblVendorName, allVendors){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View customView = super.getView(position, convertView, parent);
                TextView lblVendorName = customView.findViewById(R.id.lblVendorName);
                TextView lblVendorPhone= customView.findViewById(R.id.lblVendorPhone);
                TextView lblVendorAdded= customView.findViewById(R.id.lblVendorAdded);

                final Vendor currentVendor = allVendors.get(position);
                lblVendorName.setText(currentVendor.getName());
                lblVendorPhone.setText(currentVendor.getPhone());
                lblVendorAdded.setText(currentVendor.getDateAdded());

                //Edit vendor button stuff
                Button btnEdit = customView.findViewById(R.id.btnEdit);
                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Vendors.this, EditVendor.class);
                        i.putExtra(EditVendor.EXTRA_VENDOR_ID, currentVendor.getId());
                        startActivity(i);
                    }
                });

                //Delete vendor button stuff
                Button btnDelete = customView.findViewById(R.id.btnDelete);
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //yes/No dialogue
                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case DialogInterface.BUTTON_POSITIVE:
                                        SQLVendorDataAccess dataAccess = new SQLVendorDataAccess(Vendors.this);
                                        dataAccess.deleteVendor(currentVendor);
                                        Toast.makeText(Vendors.this, "Successfully deleted " + currentVendor.getName() + "!", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(Vendors.this, Vendors.class);
                                        startActivity(i);
                                        break;

                                    case DialogInterface.BUTTON_NEGATIVE:
                                        //No button clicked
                                        break;
                                }
                            }
                        };
                        //End yes/no

                        AlertDialog.Builder builder = new AlertDialog.Builder(Vendors.this);
                        builder.setMessage("Are you sure you want to delete " + currentVendor.getName() + "?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
                    }
                });

                //View products stuff
                Button btnViewProducts = customView.findViewById(R.id.btnViewProducts);
                btnViewProducts.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });


                return customView;
            };

        };
        theListView.setAdapter(adapter);

    }

    /**
    @Override
    public void onResume() {
        super.onResume();
        clearValues();
        populateListView();
    }
    **/
}