package com.huntershenep.hsfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    Button btnVendors;
    TextView timeDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnVendors = findViewById(R.id.btnViewVendors);
        timeDate = findViewById(R.id.txtDate);
        Date aaa = new Date();
        timeDate.setText(aaa.toString());

        btnVendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Vendors.class);
                startActivity(i);
            }
        });

    }







}