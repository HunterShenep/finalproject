package com.huntershenep.hsfinal;

import com.huntershenep.hsfinal.models.Product;

import java.util.ArrayList;

public interface Productable {

    public ArrayList<Product> pullProducts();

    public Product getProductByID(int id);

    public Product insertProduct(Product p);

    public void deleteProduct(Product p);

    public Product updateProduct(Product p);

}
