package com.huntershenep.hsfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.huntershenep.hsfinal.models.Vendor;

public class EditVendor extends AppCompatActivity {

    public static final String EXTRA_VENDOR_ID = "vendorID";
    public static final String TAG = "Vendors";
    private EditText txtVendorName;
    private EditText txtVendorPhone;
    private EditText txtVendorEmail;
    private EditText txtVendorWebsite;
    private EditText txtVendorDescription;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_vendor);
        SQLVendorDataAccess dataAccess = new SQLVendorDataAccess(this);

        txtVendorName = findViewById(R.id.txtEditVendorName);
        txtVendorPhone = findViewById(R.id.txtEditVendorPhone);
        txtVendorEmail = findViewById(R.id.txtEditVendorEmail);
        txtVendorWebsite = findViewById(R.id.txtEditVendorWebsite);
        txtVendorDescription = findViewById(R.id.txtEditVendorDescription);
        btnSubmit = findViewById(R.id.btnEditVendorSubmit);

        Intent i = getIntent();
        final int theID = i.getIntExtra(EXTRA_VENDOR_ID, 0);
        //Log.d("Vendor", Integer.toString(theID));

        final Vendor theVendor = dataAccess.getVendorByID(theID);

        txtVendorName.setText(theVendor.getName());
        txtVendorPhone.setText(theVendor.getPhone());
        txtVendorEmail.setText(theVendor.getEmail());
        txtVendorWebsite.setText(theVendor.getWebsite());
        txtVendorDescription.setText(theVendor.getDescription());

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateInput()) {
                    SQLVendorDataAccess dataAccess = new SQLVendorDataAccess(EditVendor.this);
                    dataAccess.updateVendor(new Vendor(theID, txtVendorName.getText().toString(), txtVendorPhone.getText().toString(), txtVendorEmail.getText().toString(), txtVendorWebsite.getText().toString(), txtVendorDescription.getText().toString(), theVendor.getDateAdded().toString()));
                    Toast.makeText(EditVendor.this, "Successfully updated!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(EditVendor.this, Vendors.class);
                    startActivity(i);
                }
            }
        });



    }

    public boolean validateInput(){
        boolean inputIsValid = true;
        if(txtVendorName.getText().toString().isEmpty()){
            txtVendorName.setError("You must enter a name!");
            inputIsValid = false;
        }

        if(txtVendorPhone.getText().toString().isEmpty()){
            txtVendorPhone.setError("You must enter a phone number!");
            inputIsValid = false;
        }

        if(txtVendorEmail.getText().toString().isEmpty()){
            txtVendorEmail.setError("You must enter an email!");
            inputIsValid = false;
        }

        if(txtVendorWebsite.getText().toString().isEmpty()){
            txtVendorWebsite.setError("You must enter a website!");
            inputIsValid = false;
        }

        if(txtVendorDescription.getText().toString().isEmpty()){
            txtVendorDescription.setError("You must enter a description!");
            inputIsValid = false;
        }

        return inputIsValid;
    }

}