package com.huntershenep.hsfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.huntershenep.hsfinal.models.Vendor;

import java.util.Date;

public class AddVendor extends AppCompatActivity {

    private EditText txtVendorName;
    private EditText txtVendorPhone;
    private EditText txtVendorEmail;
    private EditText txtVendorWebsite;
    private EditText txtVendorDescription;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vendor);

        txtVendorName = findViewById(R.id.txtAddVendorName);
        txtVendorPhone = findViewById(R.id.txtAddVendorPhone);
        txtVendorEmail = findViewById(R.id.txtAddVendorEmail);
        txtVendorWebsite = findViewById(R.id.txtAddVendorWebsite);
        txtVendorDescription = findViewById(R.id.txtAddVendorDescription);
        btnSubmit = findViewById(R.id.btnAddVendorSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLVendorDataAccess dataAccess = new SQLVendorDataAccess(AddVendor.this);
                Date today = new Date();


                if(validateInput()) {
                    Vendor theVendor = new Vendor(txtVendorName.getText().toString(), txtVendorPhone.getText().toString(), txtVendorEmail.getText().toString(),
                            txtVendorWebsite.getText().toString(), txtVendorDescription.getText().toString(), today.toString());
                    dataAccess.insertVendor(theVendor);
                    Toast.makeText(AddVendor.this, "SUCCESS!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(AddVendor.this, Vendors.class);
                    startActivity(i);
                }

            }
        });
    }

    public boolean validateInput(){
        boolean inputIsValid = true;
        if(txtVendorName.getText().toString().isEmpty()){
            txtVendorName.setError("You must enter a name!");
            inputIsValid = false;
        }

        if(txtVendorPhone.getText().toString().isEmpty()){
            txtVendorPhone.setError("You must enter a phone number!");
            inputIsValid = false;
        }

        if(txtVendorEmail.getText().toString().isEmpty()){
            txtVendorEmail.setError("You must enter an email!");
            inputIsValid = false;
        }

        if(txtVendorWebsite.getText().toString().isEmpty()){
            txtVendorWebsite.setError("You must enter a website!");
            inputIsValid = false;
        }

        if(txtVendorDescription.getText().toString().isEmpty()){
            txtVendorDescription.setError("You must enter a description!");
            inputIsValid = false;
        }

        return inputIsValid;
    }

}