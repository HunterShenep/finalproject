package com.huntershenep.hsfinal;

import com.huntershenep.hsfinal.models.Vendor;

import java.util.ArrayList;

public interface Vendorable {

    public ArrayList<Vendor> pullAllVendors();

    public Vendor getVendorByID(int id);

    public Vendor insertVendor(Vendor v);

    public void deleteVendor(Vendor v);

    public Vendor updateVendor(Vendor v);

}
