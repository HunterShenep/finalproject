package com.huntershenep.hsfinal;

import android.content.Context;

import com.huntershenep.hsfinal.models.Vendor;
import com.huntershenep.hsfinal.sqlite.MySQLiteHelper;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class SQLVendorDataAccess implements Vendorable{

    private static final String TAG = "SQLVendorDataAccess";
    private ArrayList<Vendor> allVendors = new ArrayList<>();
    private Context context;
    private MySQLiteHelper dbHelper;
    private SQLiteDatabase database;

    public static final String TABLE_NAME = "vendors";
    public static final String COLUMN_VENDOR_ID = "_id";
    public static final String COLUMN_VENDOR_NAME = "vendor_name";
    public static final String COLUMN_VENDOR_PHONE = "vendor_phone";
    public static final String COLUMN_VENDOR_EMAIL = "vendor_email";
    public static final String COLUMN_VENDOR_WEBSITE = "vendor_website";
    public static final String COLUMN_VENDOR_DESCRIPTION = "vendor_description";
    public static final String COLUMN_VENDOR_DATE_ADDED= "vendor_date_added";

    public static final String TABLE_CREATE = String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT )",
            TABLE_NAME,
            COLUMN_VENDOR_ID,
            COLUMN_VENDOR_NAME,
            COLUMN_VENDOR_PHONE,
            COLUMN_VENDOR_EMAIL,
            COLUMN_VENDOR_WEBSITE,
            COLUMN_VENDOR_DESCRIPTION,
            COLUMN_VENDOR_DATE_ADDED
    );


    public SQLVendorDataAccess(Context context){
        Log.d(TAG, "Instantiating SQLVEndorDataAcess");
        this.context = context;
        this.dbHelper = new MySQLiteHelper(context);
        this.database = dbHelper.getWritableDatabase();

    }

    @Override
    public ArrayList<Vendor> pullAllVendors() {

        String query = "select * from " + TABLE_NAME;
        Cursor c = database.rawQuery(query, null);

        if(c != null && c.getCount() > 0){
            c.moveToFirst();
            while(!c.isAfterLast()){
                int id = c.getInt(0);
                String name = c.getString(1);
                String phone = c.getString(2);
                String email = c.getString(3);
                String website = c.getString(4);
                String desc = c.getString(5);
                String added = c.getString(6);
                Vendor theVendor = new Vendor(id, name, phone, email, website, desc, added);
                Log.d(TAG, "Yooooo THE NAME: " + theVendor.getName());
                allVendors.add(theVendor);
                c.moveToNext();
            }
        }

        return allVendors;

    }

    @Override
    public Vendor getVendorByID(int idd) {
        Vendor theVendor = new Vendor();
        String query = "select * from " + TABLE_NAME + " where _id='" + idd + "'";
        Cursor c = database.rawQuery(query, null);


            if(c.moveToFirst()) {

                theVendor.setId(c.getInt(0));
                theVendor.setName(c.getString(1));
                theVendor.setPhone(c.getString(2));
                theVendor.setEmail(c.getString(3));
                theVendor.setWebsite(c.getString(4));
                theVendor.setDescription(c.getString(5));
                theVendor.setDateAdded(c.getString(6));
                c.moveToNext();

            }

        return theVendor;
    }

    @Override
    public Vendor insertVendor(Vendor v) {
        try {
            String statement = "insert into " + TABLE_NAME + " ";
            statement += "values (NULL, ";
            statement += surround(v.getName());
            statement += surround(v.getPhone());
            statement += surround(v.getEmail());
            statement += surround(v.getWebsite());
            statement += surround(v.getDescription());
            statement += surround(v.getDateAdded(), true);
            statement += ");";
            database.execSQL(statement);
            Log.d(TAG, "Success insert Vendor?????????");
        }
        catch(Exception e){

        }
        return v;
    }

    @Override
    public void deleteVendor(Vendor v) {
        String query = "delete from " + TABLE_NAME + " where _id='" + v.getId() + "'";
        database.execSQL(query);
    }

    @Override
    public Vendor updateVendor(Vendor v) {
        String queryName = "update " + TABLE_NAME + " set " + COLUMN_VENDOR_NAME + "='" + v.getName() + "' where _id='" + v.getId() + "'";
        String queryPhone = "update " + TABLE_NAME + " set " + COLUMN_VENDOR_PHONE + "='" + v.getPhone() + "' where _id='" + v.getId() + "'";
        String queryEmail = "update " + TABLE_NAME + " set " + COLUMN_VENDOR_EMAIL + "='" + v.getEmail() + "' where _id='" + v.getId() + "'";
        String queryWebsite = "update " + TABLE_NAME + " set " + COLUMN_VENDOR_WEBSITE + "='" + v.getWebsite() + "' where _id='" + v.getId() + "'";
        String queryDesc = "update " + TABLE_NAME + " set " + COLUMN_VENDOR_DESCRIPTION + "='" + v.getDescription() + "' where _id='" + v.getId() + "'";
        database.execSQL(queryName);
        database.execSQL(queryPhone);
        database.execSQL(queryEmail);
        database.execSQL(queryWebsite);
        database.execSQL(queryDesc);
        return v;
    }

    public boolean createTable(){
        try {
            database.execSQL(TABLE_CREATE);
        }
        catch(Exception e){

        }

        Log.d(TAG, "299239 ################################");
        return true;
    }

    public String surround(String text){
        return ("'" + text + "',");
    }

    public String surround(String text, boolean last){
        return ("'" + text + "'");
    }
}
