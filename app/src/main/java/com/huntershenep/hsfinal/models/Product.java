package com.huntershenep.hsfinal.models;

public class Product {

    private int productID;
    private String name;
    private String description;
    private long price;

    public Product(String name, String description, long price){
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public String toString(){
        String msg = "";
        msg += "ID: " + this.getProductID();
        msg += " | Name: " + this.getName() + "\n";
        msg += "Description: " + this.getDescription() + "\n";
        msg += "Price: " + this.getPrice();
        return msg;
    }

    public long getProductID() {
        return productID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
