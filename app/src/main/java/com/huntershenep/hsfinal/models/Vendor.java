package com.huntershenep.hsfinal.models;

import java.util.Date;

public class Vendor {

    private int id;
    private String name;
    private String phone;
    private String email;
    private String website;
    private String description;
    private String dateAdded;


    //Vendor constructor
    public Vendor(){}

    public Vendor (String name, String phone, String email, String website, String description, String dateAdded){
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.website = website;
        this.description = description;
        this.dateAdded = dateAdded;
    }

    public Vendor (int id, String name, String phone, String email, String website, String description, String dateAdded){
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.website = website;
        this.description = description;
        this.dateAdded = dateAdded;
    }

    //toString override
    @Override
    public String toString(){
        String msg = "";
        msg += "ID: " + this.getId();
        msg += " | Name: " + this.getName() + "\n";
        msg += "Phone: " + this.getPhone();
        msg += " | Email: " + this.getEmail() + "\n";
        msg += "Website: " + this.getWebsite();
        msg += " | DateAdded: " + this.getDateAdded().toString() + "\n";
        msg += "Description: " + this.getDescription();
        return msg;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }
}
